<?php
/**
 * Created by PhpStorm.
 * User: Wojtek
 * Date: 2015-04-22
 * Time: 12:56
 */
require_once("FactorialDigitSum.php");

class FactorialDigitSumTest extends PHPUnit_Framework_TestCase {

    public function testFactorialDigitSum()
    {
        $obj = new FactorialDigitSum(10);
        $a=$obj->factorial();

        $this->assertEquals(3628800, $a);

        $b=$obj->sumDigits();

        $this->assertEquals(27, $b);

        $objB = new FactorialDigitSum(5);

        $a=$objB->factorial();
        $this->assertEquals(120, $a);

        $b=$objB->sumDigits();

        $this->assertEquals(3, $b);
    }

}
