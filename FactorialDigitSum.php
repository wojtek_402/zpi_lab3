<?php
/**
 * This class contains simple methods for calculating sum of digits in factored number
 * @author Wojciech Król <wk20981@gmail.com>
 */
class FactorialDigitSum
{
    /**
     *  @access private
     *  @var int Inputed integer variable to calculate.
     */
    private $InputNumber = null;

    /**
     *  @param int $number Natural number for other calculating methods
     *  @exception Exception when inputed number is invalid or is lower than 0
     *  @example
     *
     *  $object = new FactorialDigitSum(-1);
     *
     *  throws:
     *  Exception("Required natural number (>= 0).");
     *
     *  $object = new FactorialDigitSum(10);
     *  return $object->sumDigits();
     *
     *  Returns:
     *  27
     *
     */
    public function __construct($number)
    {
        if(!is_int($number))
            throw new Exception("Invalid input.");
        else if($number < 0)
            throw new Exception("Required natural number (>= 0).");

        $this->InputNumber = $number;
    }

    /**
     * This method is summing digits of inputed number. This method gets factorial of this number and calculating sum of digits.
     * @access public
     * @return Sum of digits
     *
     * Example:
     *
     *  $object = new FactorialDigitSum(10)
     *  return $object->sumDigits();
     *
     *  Returns:
     *  27
     */
    public function sumDigits()
    {
        $number = $this->factorial( $this->InputNumber );
        $sum = 0;
        do {
            $sum += $number % 10;
        } while($number /= 10);
        return $sum;
    }

    /**
     * Calculating factorial of number. This method is using private variable of this object.
     * @access private
     * @exception Exception when number is lower than 0
     * @return Factorial of number
     *
     * @example
     *  (...)
     *  $this->InputNumber = 10;
     *  (...)
     *  return $this->factorial();
     *
     *  Returns:
     *  3628800
     */
    public function factorial()
    {
        $factorial = 1;
        for($i = 1; $i <= $this->InputNumber; $i++)
        {
            $factorial *= $i;
        }

        return $factorial;
    }
}

try
{
    // Init number and send it to object
    $number = 10;
    $FactorialDigitSumObject = new FactorialDigitSum($number);

    // Sum digits
    $sum = $FactorialDigitSumObject->sumDigits();

    // Display results
    echo "Input number: ".$number."<br />";
    echo "Sum of digits: ".$sum;
}
catch(Exception $e)
{
    // Handle exception
    exit("Got exception: ".$e->getMessage());
}